package org.example;

public final class CourseStatus {
    public static final CourseStatus active = new CourseStatus("Active");
    public static final CourseStatus deactive = new CourseStatus("Deactive");

    public final String name;

    private CourseStatus(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }
    public static String findByName(String name){
        if(active.name()==name || deactive.name()==name){
            return name;
        }
        else
            return null;
    }
}
