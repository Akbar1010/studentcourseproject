package org.example;

public class Main {
    public static void main(String[] args) {
        Course course = new Course(CourseStatus.active,"Math",5,new String[]{"divide","multiply"});
        Course course1 = new Course(CourseStatus.deactive,"Chemistry",3,new String[]{"molecule","atom"});
        Course course2 = new Course(CourseStatus.active,"History",4,new String[]{"war","chronology"});
        Student student = new Student("Akbar",20,3,new Course[]{course,course2,course2});
    }
}