package org.example;

public class Course {
    private CourseStatus status;
    private String name;
    private int credit;
    private String[] keywords;

    public Course(CourseStatus status, String name, int credit, String[] keywords) {
        this.status = status;
        this.name = name;
        this.credit = credit;
        this.keywords = keywords;
    }
    public Course(){

    }

    public CourseStatus getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public int getCredit() {
        return credit;
    }

    public String[] getKeywords() {
        return keywords;
    }

    public void setStatus(CourseStatus status) {
        this.status = status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }
}
